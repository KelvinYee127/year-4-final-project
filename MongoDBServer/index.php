<?php
    //connect to db
    $connection = new Mongo("mongodb://localhost:27017");

    //get db
    $dbname = $connection->selectDB('energymonitor');

    //get colelction in db
    $collection = $dbname->selectCollection('user');
    
    //echo $_POST['email'] . $_POST['deviceNumber'] . $_POST['password'];
    
    //echo $_POST['firstname'] . $_POST['lastname'];
    
    if (isset($_POST['email']) && isset($_POST['deviceNumber']) && isset($_POST['password'])) {
        $email = $_POST['email'];
        $deviceNumber = $_POST['deviceNumber'];
        $password = $_POST['password'];
        //$id = $_POST['_id'];
        
        $newUser = array(
            //"_id" => $id,
            "email" => $email,
            "deviceNumber" => $deviceNumber,
            "password" => $password
        );

        $collection->insert($newUser);
        
        //testing to send json response back to android
        {
            $json[]= array(
                //"_id" => $id,
                "email" => $email,
                "deviceNumber" => $deviceNumber,
                "password" => $password
            );
        }

        $jsonstring = json_encode($json);
        echo $jsonstring;
    }else{
        echo "no data";
    }
    
//    //find all document0 in the collection
//    $documents = $collection->find();
//
//    //loop through all collection and print value of 'name' attribute
//    foreach ($documents as $document){
//        echo $document["name"] . '\n';
//    }
//
//    //create new user object
//    $newUser = array(
//        "name" => "wendy"
//    );
//
//    //insert into collection
//    $collection->insert($newUser);
?>
