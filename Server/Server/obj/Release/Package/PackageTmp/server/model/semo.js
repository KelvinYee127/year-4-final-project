"use strict";
class SEMO {
    constructor(currency, tradeDate, deliveryDate, deliveryHour, deliveryInterval, runType, smp, lambda, systemLoad, cmsTimeStamp) {
        this.currency = currency;
        this.tradeDate = tradeDate;
        this.deliveryDate = deliveryDate;
        this.deliveryHour = deliveryHour;
        this.deliveryInterval = deliveryInterval;
        this.runType = runType;
        this.smp = smp;
        this.lambda = lambda;
        this.systemLoad = systemLoad;
        this.cmsTimeStamp = cmsTimeStamp;
    }
}
exports.SEMO = SEMO;
