"use strict";
const xml2js = require('xml2js');
const semo_1 = require('../model/semo');
class SEMODataParser {
    constructor() {
        this.parseSemoData = (xmlStr, callback) => {
            var parser = new xml2js.Parser();
            //Parse XML data into SEMO objects and store them into an array
            parser.parseString(xmlStr, function (error, semo) {
                var semoArray = new Array();
                for (var i = 0; i < 384; i += 2) {
                    var semoObject = new semo_1.SEMO(semo.EP2_RESULTS.Table1[i].CURRENCY_FLAG.toString(), semo.EP2_RESULTS.Table1[i].TRADE_DATE.toString(), semo.EP2_RESULTS.Table1[i].DELIVERY_DATE.toString(), semo.EP2_RESULTS.Table1[i].DELIVERY_HOUR.toString(), semo.EP2_RESULTS.Table1[i].DELIVERY_INTERVAL.toString(), semo.EP2_RESULTS.Table1[i].RUN_TYPE.toString(), semo.EP2_RESULTS.Table1[i].SMP.toString(), semo.EP2_RESULTS.Table1[i].LAMBDA.toString(), semo.EP2_RESULTS.Table1[i].SYSTEM_LOAD.toString(), semo.EP2_RESULTS.Table1[i].CMS_TIME_STAMP.toString());
                    semoArray.push(semoObject);
                }
                console.log('array size: ', semoArray.length);
                callback(semoArray);
            });
        };
    }
}
exports.SEMODataParser = SEMODataParser;
