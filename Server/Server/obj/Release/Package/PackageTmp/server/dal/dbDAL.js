"use strict";
var MongoClient = require('mongodb').MongoClient;
class DBDAL {
    constructor() {
        //Opens a connection to mongodb
        this.openDBConnection = (callback) => {
            MongoClient.connect('mongodb://ema:ema127@ds056559.mlab.com:56559/energymonitor', (error, db) => {
                //MongoClient.connect('mongodb://localhost:27017/energymonitor', (error, db: mongodb.Db) => {
                if (error) {
                    console.log('Unable to connect to server.', error);
                }
                else {
                    console.log('Connection to server success.');
                    this.database = db;
                    callback();
                }
            });
        };
        //Closes the database connection
        this.closeDBConnection = () => {
            this.database.close();
        };
    }
}
exports.DBDAL = DBDAL;
