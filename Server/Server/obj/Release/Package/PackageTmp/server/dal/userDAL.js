"use strict";
class UserDAL {
    //Makes a reference to the database dal to use the connection
    constructor(dbDAL) {
        //Get user by device number from the database
        this.getUserByDeviceNumber = (deviceNumber, callback) => {
            this.dbDAL.database.collection('user').findOne({ deviceNumber: deviceNumber })
                .then(callback);
        };
        this.insertUser = (jsonArray) => {
            this.dbDAL.database.collection('user').insertMany(jsonArray);
        };
        this.dbDAL = dbDAL;
    }
}
exports.UserDAL = UserDAL;
