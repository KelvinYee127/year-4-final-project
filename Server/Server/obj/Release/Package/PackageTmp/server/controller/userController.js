"use strict";
class UserController {
    constructor(userDAL) {
        this.insertUser = (jsonArray) => {
            this.userDAL.insertUser(jsonArray);
        };
        this.getUserByDeviceNumber = (deviceNumber, callback) => {
            this.userDAL.getUserByDeviceNumber(deviceNumber, callback);
        };
        //Method receive post request from android app
        this.login = (req, res) => {
            var email = req.body.email;
            var deviceNumber = req.body.deviceNumber;
            var password = req.body.password;
            console.log(email + " " + deviceNumber + " " + password);
            //Calls get user by device number to check if log in details are valid
            this.getUserByDeviceNumber(deviceNumber, (user) => {
                var verified;
                //All 3 fields must match the database value to be verified
                if (email == user.email && deviceNumber == user.deviceNumber && password == user.password) {
                    verified = true;
                }
                else {
                    verified = false;
                }
                console.log("User verified: " + verified);
                //Sends a boolean object back to app
                res.jsonp({ verified: verified });
            });
        };
        this.userDAL = userDAL;
    }
}
exports.UserController = UserController;
