"use strict";
const oem_1 = require('../model/oem');
const https = require('https');
class OEMController {
    constructor(oemDAL) {
        //Method receive post request from android app
        this.getOEMData = (req, response) => {
            console.log("OEM request: ", req.body.apikey);
            var URL = 'https://emoncms.org/feed/list.json&apikey=' + req.body.apikey;
            console.log("URL: ", URL);
            //Fires a https request to get OEM data
            https.get(URL, (res) => {
                var body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', () => {
                    //Parse string response JSON
                    var oemResponse = JSON.parse(body);
                    var date = new Date(oemResponse[2].time * 1000);
                    console.log("Time here: ", date);
                    //There are 3 objects in the response: Power, Temperature and Power kWh
                    //Power kWh is the usage so get it from the response which is object 3/index 2 and make it into an OEM object
                    var oemObject = new oem_1.OEM("d5522330", oemResponse[0].name.toString(), date /*oemResponse[2].time*/, oemResponse[0].value);
                    //Puts the OEM object into database
                    this.oemDAL.storeOEMData(oemObject);
                    //Sends the OEM object back to the app
                    response.jsonp(oemObject);
                });
            }).on('error', function (e) {
                console.log("Got an error on get oem: ", e);
            });
        };
        this.getOEMData1 = () => {
            var URL = 'https://emoncms.org/feed/list.json&apikey=a9094f51ec8a9d3b295ba636e53be021';
            console.log("URL: ", URL);
            //Fires a https request to get OEM data
            https.get(URL, (res) => {
                var body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', () => {
                    //Parse string response JSON
                    var oemResponse = JSON.parse(body);
                    var date = new Date(oemResponse[0].time * 1000);
                    console.log("Time here: ", date);
                    //There are 3 objects in the response: Power, Temperature and Power kWh
                    //Power kWh is the usage so get it from the response which is object 3/index 2 and make it into an OEM object
                    //var oemObject = new OEM("d5522330", oemResponse[0].name.toString(), date, oemResponse[0].value);
                    //Puts the OEM object into database
                    //this.oemDAL.storeOEMData(oemObject);
                });
            }).on('error', function (e) {
                console.log("Got an error on get oem: ", e);
            });
        };
        this.getUserUsage = (req, res) => {
            var deviceNumber = req.body.deviceNumber;
            this.oemDAL.getUserUsage(deviceNumber, (usage) => {
                console.log("usage: ", usage[usage.length - 1]);
                res.jsonp(usage);
            });
        };
        this.oemDAL = oemDAL;
    }
}
exports.OEMController = OEMController;
