"use strict";
const semoDataParser_1 = require('../parser/semoDataParser');
const http = require('http');
class SemoController {
    constructor(semoDAL) {
        //Method receive post request from android app
        this.getSemoData = (req, response) => {
            console.log("semo request: ", req.body.DatasetName);
            var URL = 'http://semorep.sem-o.com/DataCollection/DataSets.asmx/queryDatasetXML?DatasetName='
                + req.body.DatasetName + '&User=' + req.body.User + '&Password=' + req.body.Password
                + '&FromDate=' + req.body.FromDate + '&ToDate=' + req.body.ToDate + '&P1=' + req.body.P1 + '&P2=' + req.body.P2
                + '&P3=' + req.body.P3 + '&P4=' + req.body.P4 + '&P5=' + req.body.P5;
            console.log("URL: ", URL);
            //Fires a http request to get semo data
            http.get(URL, (res) => {
                var body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', () => {
                    //Sends the response to an XML parser
                    var semoDataParser = new semoDataParser_1.SEMODataParser();
                    semoDataParser.parseSemoData(body, (semoArray) => {
                        //Sends the parsed data which is an array of SEMO objects to the database to be stored.
                        this.semoDAL.storeSemoData(semoArray);
                        //Sends the array back to the app.
                        response.jsonp(semoArray);
                    });
                });
            }).on('error', function (e) {
                console.log("Got an error on get semo: ", e);
            });
        };
        this.getSemoData1 = (fromDate, toDate) => {
            //var URL: string = 'http://semorep.sem-o.com/DataCollection/DataSets.asmx/queryDatasetXML?DatasetName='
            //    + req.body.DatasetName + '&User=' + req.body.User + '&Password=' + req.body.Password
            //    + '&FromDate=' + req.body.FromDate + '&ToDate=' + req.body.ToDate + '&P1=' + req.body.P1 + '&P2=' + req.body.P2
            //    + '&P3=' + req.body.P3 + '&P4=' + req.body.P4 + '&P5=' + req.body.P5;
            var URL = 'http://semorep.sem-o.com/DataCollection/DataSets.asmx/queryDatasetXML?' +
                'DatasetName=EP2_RESULTS&User=kelvin-049@hotmail.com&Password=o212345&' +
                'FromDate=' + fromDate + '&ToDate=' + toDate + '&P1=EP2&P2=&P3=&P4=&P5=';
            console.log("URL: ", URL);
            //Fires a http request to get semo data
            http.get(URL, (res) => {
                var body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', () => {
                    //Sends the response to an XML parser
                    var semoDataParser = new semoDataParser_1.SEMODataParser();
                    semoDataParser.parseSemoData(body, (semoArray) => {
                        //Sends the parsed data which is an array of SEMO objects to the database to be stored.
                        this.semoDAL.storeSemoData(semoArray);
                        //Sends the array back to the app.
                        //response.jsonp(semoArray);
                    });
                });
            }).on('error', function (e) {
                console.log("Got an error on get semo: ", e);
            });
        };
        this.semoDAL = semoDAL;
    }
}
exports.SemoController = SemoController;
