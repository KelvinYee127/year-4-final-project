"use strict";
const express = require('express');
const routes = require('./routes/index');
const http = require('http');
const path = require('path');
const userDAL_1 = require('./server/dal/userDAL');
const semoDAL_1 = require('./server/dal/semoDAL');
const oemDAL_1 = require('./server/dal/oemDAL');
const userController_1 = require('./server/controller/userController');
const semoController_1 = require('./server/controller/semoController');
const oemController_1 = require('./server/controller/oemController');
const dbDAL_1 = require('./server/dal/dbDAL');
var app = express();
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
const stylus = require('stylus');
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}
app.get('/', routes.index);
app.get('/about', routes.about);
app.get('/contact', routes.contact);
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
//Initialise database DAL object
var dbDAL = new dbDAL_1.DBDAL();
//Open connection to mongoDB
dbDAL.openDBConnection(() => {
    //Initialise user DAL object by passing in database DAL object
    var userDAL = new userDAL_1.UserDAL(dbDAL);
    //Initialise user controller object by passing in user DAL object
    var userController = new userController_1.UserController(userDAL);
    //Initialise semo DAL object by passing in database DAL object
    var semoDAL = new semoDAL_1.SemoDAL(dbDAL);
    //Initialise semo controller object by passing in semo DAL object
    var semoController = new semoController_1.SemoController(semoDAL);
    //Initialise oem DAL object by passing in database DAL object
    var oemDAL = new oemDAL_1.OEMDAL(dbDAL);
    //Initialise oem controller object by passing in oem DAL object
    var oemController = new oemController_1.OEMController(oemDAL);
    //Specify routing for different URLs
    app.post('/login', userController.login);
    app.post('/getSemo', semoController.getSemoData);
    app.post('/getOEM', oemController.getOEMData);
    app.post('/getUserUsage', oemController.getUserUsage);
    setInterval(oemController.getOEMData1, 60000);
});
//semoController.getSemoData1('20170220', '20170224'); 
