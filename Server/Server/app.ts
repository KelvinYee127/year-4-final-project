﻿import express = require('express');
import routes = require('./routes/index');
import http = require('http');
import path = require('path');
import mongodb = require('mongodb');
import {UserDAL} from './server/dal/userDAL';
import {SemoDAL} from './server/dal/semoDAL';
import {OEMDAL} from './server/dal/oemDAL';
import {UserController} from './server/controller/userController';
import {SemoController} from './server/controller/semoController';
import {OEMController} from './server/controller/oemController';
import {DBDAL} from './server/dal/dbDAL';

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);

import stylus = require('stylus');
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/about', routes.about);
app.get('/contact', routes.contact);


http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

//Initialise database DAL object
var dbDAL = new DBDAL();
//Open connection to mongoDB
dbDAL.openDBConnection(() => {
    //Initialise user DAL object by passing in database DAL object
    var userDAL = new UserDAL(dbDAL);
    //Initialise user controller object by passing in user DAL object
    var userController = new UserController(userDAL);

    //Initialise semo DAL object by passing in database DAL object
    var semoDAL = new SemoDAL(dbDAL);
    //Initialise semo controller object by passing in semo DAL object
    var semoController = new SemoController(semoDAL);

    //Initialise oem DAL object by passing in database DAL object
    var oemDAL = new OEMDAL(dbDAL);
    //Initialise oem controller object by passing in oem DAL object
    var oemController = new OEMController(oemDAL);

    //Specify routing for different URLs
    app.post('/login', userController.login);
    app.post('/getUserUsageOneHour', oemController.calculateUsageForLastOneHour);
    app.post('/getPrice', semoController.getPrice);
    app.post('/changePassword', userController.changePassword);
    app.post('/getHistoryData', oemController.getHistoryData);
    app.post('/getPayable', oemController.getPayableForOneDay);

    setInterval(oemController.getOEMData, 30000);
    //semoController.getSemoData('20170426', '20170426');
});