﻿import {OEMDAL} from '../dal/oemDAL';
import {OEM} from '../model/oem';
import https = require('https');
import express = require('express');

export class OEMController {

    public oemDAL: OEMDAL;

    constructor(oemDAL: OEMDAL) {
        this.oemDAL = oemDAL;
    }

    public getOEMData = () => {
        var URL: string = 'https://emoncms.org/feed/list.json&apikey=a9094f51ec8a9d3b295ba636e53be021';

        console.log("URL: ", URL);
        
        //Fires a https request to get OEM data
        https.get(URL, (res) => {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });

            res.on('end', () => {
                //Parse string response JSON
                var oemResponse = JSON.parse(body);

                var date = new Date(oemResponse[0].time * 1000);

                console.log("Time here: ", date);

                //There are 3 objects in the response: Power, Temperature and Power kWh
                //Power kWh is the usage so get it from the response which is object 3/index 2 and make it into an OEM object
                var oemObject = new OEM("d5522330", oemResponse[0].name.toString(), date, parseInt(oemResponse[0].value));

                //Puts the OEM object into database
                this.oemDAL.storeOEMData(oemObject);
            });
        }).on('error', function (e) {
            console.log("Got an error on get oem: ", e);
        });
    }

    public getUserUsage = (req: express.Request, res: express.Response) => {
        var deviceNumber = req.body.deviceNumber;

        this.oemDAL.getUserUsage(deviceNumber, (usage) => {
            console.log("usage: ", usage[usage.length - 1]);
            res.jsonp({usage: usage[usage.length - 1].value});
        });
    }

    public calculateUsageForLastOneHour = (req: express.Request, res: express.Response) => {
        var deviceNumber = req.body.deviceNumber;

        this.oemDAL.getUserUsage(deviceNumber, (usage) => {
            var averageUsageInOneHour = 0;

            for (var i = usage.length - 1; i > usage.length - 121; i--) {
                averageUsageInOneHour += usage[i].value;
            }

            averageUsageInOneHour /= 120;
            averageUsageInOneHour = Math.round(averageUsageInOneHour);

            console.log("Average Usage: ", averageUsageInOneHour);
            res.jsonp({ usage: averageUsageInOneHour });
        });
    }

    public getHistoryData = (req: express.Request, res: express.Response) => {
        var deviceNumber = req.body.deviceNumber;
        var selection = req.body.selection;

        var currentDate = new Date();
        var limit = 0;

        this.oemDAL.getUserUsage(deviceNumber, (usage) => {
            console.log('usage length: ', usage.length);
            if (selection == "day") {
                var usageArrayHourly = [];
                var count = 0;
                var averageUsageInOneHour = 0;
                var currentHour = currentDate.getHours();
                var oldCurrentHour = currentHour;

                //120 per hour * 24 hours
                if (usage.length > 2880) {
                    limit = usage.length - 2881;
                }

                for (var i = usage.length - 1; i > limit; i--) {
                    averageUsageInOneHour += usage[i].value;
                    count++;

                    if ((count % 120) == 0) {
                        if (currentHour == -1) {
                            currentHour = 23;
                        }

                        averageUsageInOneHour /= 120;
                        averageUsageInOneHour = Math.round(averageUsageInOneHour);

                        var resultStr = currentHour.toString() + "," + averageUsageInOneHour.toString();
                        console.log("Result: ", resultStr);

                        usageArrayHourly.push(resultStr);
                        count = 0;
                        averageUsageInOneHour = 0;
                        currentHour--;
                    }
                }
                res.jsonp({ usage: usageArrayHourly });
            } else if (selection == "week") {
                var usageArrayDaily = [];
                var hourCount = 0;
                var dayCount = 0;
                var averageUsageInOneHour = 0;
                var averageUsageInOneDay = 0;
                var currentDay = currentDate.getDay();
                var oldCurrentDay = currentDay;

                //120 per hour * 24 hours * 7 days
                if (usage.length > 20160) {
                    limit = usage.length - 20161;
                }

                for (var i = usage.length - 1; i > limit; i--) {
                    averageUsageInOneHour += usage[i].value;
                    hourCount++;
                    dayCount++;

                    if ((hourCount % 120) == 0) {
                        averageUsageInOneHour /= 120;
                        averageUsageInOneDay += averageUsageInOneHour;
                        averageUsageInOneHour = 0;
                        hourCount = 0;
                    }

                    if ((dayCount % 2880) == 0) {
                        if (currentDay == -1) {
                            currentDay = 6;
                        }

                        var dayStr;

                        if (currentDay == 0) {
                            dayStr = "Sunday";
                        } else if (currentDay == 1) {
                            dayStr = "Monday";
                        } else if (currentDay == 2) {
                            dayStr = "Tuesday";
                        } else if (currentDay == 3) {
                            dayStr = "Wednesday";
                        } else if (currentDay == 4) {
                            dayStr = "Thursday";
                        } else if (currentDay == 5) {
                            dayStr = "Friday";
                        } else if (currentDay == 6) {
                            dayStr = "Saturday";
                        }
                        console.log("Total usage in one day: ", averageUsageInOneDay);

                        averageUsageInOneDay = Math.round(averageUsageInOneDay);

                        var resultStr = dayStr + "," + averageUsageInOneDay.toString();
                        console.log("Result: ", resultStr);

                        usageArrayDaily.push(resultStr);
                        dayCount = 0;
                        averageUsageInOneDay = 0;
                        currentDay--;
                    }
                }
                res.jsonp({ usage: usageArrayDaily });
            } else if (selection == "year") {

            }
        });
    }

    public getPayableForOneDay = (req: express.Request, res: express.Response) => {
        var deviceNumber = req.body.deviceNumber;

        this.oemDAL.getUserUsage(deviceNumber, (usage) => {
            var currentDate = new Date();
            this.oemDAL.getPrice(currentDate, (price) => {

                var currentHour = currentDate.getHours();
                var priceIndex = price.length - 1;
                var usageIndex = usage.length - 1;
                var totalToPay = 0;

                for (var i = 0; i < currentHour; i++) {
                    var averageUsageInOneHour = 0;

                    for (var j = 0; j < 120; j++) {
                        averageUsageInOneHour += usage[usageIndex].value;
                        usageIndex--;
                    }

                    var averageUsageInOneHourInKW = averageUsageInOneHour / 1000;
                    var payInOneHour = averageUsageInOneHourInKW * price[priceIndex].smp;
                    totalToPay += payInOneHour;

                    priceIndex -= 2;
                }
                var totalToPayStr = (totalToPay / 100).toFixed(2);
                totalToPay = parseFloat(totalToPayStr);
                console.log("Payable: ", totalToPay);
                res.jsonp({ payable: totalToPay });     
            });
        });
    }
}