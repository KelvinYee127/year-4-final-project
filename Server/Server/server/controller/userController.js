"use strict";
class UserController {
    constructor(userDAL) {
        this.insertUser = (jsonArray) => {
            this.userDAL.insertUser(jsonArray);
        };
        this.getUserByDeviceNumber = (deviceNumber, callback) => {
            this.userDAL.getUserByDeviceNumber(deviceNumber, callback);
        };
        //Method receive post request from android app
        this.login = (req, res) => {
            var email = req.body.email;
            var deviceNumber = req.body.deviceNumber;
            var password = req.body.password;
            console.log(email + " " + deviceNumber + " " + password);
            var bcrypt = require('bcrypt');
            //Calls get user by device number to check if log in details are valid
            this.getUserByDeviceNumber(deviceNumber, (user) => {
                var verified;
                var passwordHashMatched = bcrypt.compareSync(password, user.password);
                //All 3 fields must match the database value to be verified
                if (email == user.email && deviceNumber == user.deviceNumber && passwordHashMatched) {
                    verified = true;
                }
                else {
                    verified = false;
                }
                console.log("User verified: " + verified);
                //Sends a boolean object back to app
                res.jsonp({ verified: verified });
            });
        };
        //Method receive post request from android app
        this.changePassword = (req, res) => {
            var deviceNumber = req.body.deviceNumber;
            var password = req.body.password;
            console.log(deviceNumber + " " + password);
            var bcrypt = require('bcrypt');
            var salt = bcrypt.genSaltSync(10);
            var hash = bcrypt.hashSync(password, salt);
            this.userDAL.changePassword(deviceNumber, hash, (result) => {
                if (result.result.nModified == 1) {
                    console.log("password change success");
                    res.jsonp({ changed: true });
                }
                else {
                    console.log("password change failed");
                    res.jsonp({ changed: false });
                }
            });
        };
        this.userDAL = userDAL;
    }
}
exports.UserController = UserController;
