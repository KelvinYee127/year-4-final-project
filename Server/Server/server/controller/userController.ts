﻿import {UserDAL} from '../dal/userDAL';
import {User} from '../model/user';
import {IUser} from '../interface/userInterface';
import express = require('express');

export class UserController {

    public userDAL: UserDAL;

    constructor(userDAL: UserDAL) {
        this.userDAL = userDAL;
    }

    public insertUser = (jsonArray: Array<User>) => {
        this.userDAL.insertUser(jsonArray);
    }

    public getUserByDeviceNumber = (deviceNumber: string, callback: (user: User) => void) => {
        this.userDAL.getUserByDeviceNumber(deviceNumber, callback);
    }

    //Method receive post request from android app
    public login = (req: express.Request, res: express.Response) => {
        var email = req.body.email;
        var deviceNumber = req.body.deviceNumber;
        var password = req.body.password;

        console.log(email + " " + deviceNumber + " " + password);

        var bcrypt = require('bcrypt');

        //Calls get user by device number to check if log in details are valid
        this.getUserByDeviceNumber(deviceNumber, (user: User) => {
            
            var verified: boolean;
            var passwordHashMatched: boolean = bcrypt.compareSync(password, user.password);

            //All 3 fields must match the database value to be verified
            if (email == user.email && deviceNumber == user.deviceNumber && passwordHashMatched) {
                verified = true;
            } else {
                verified = false;
            }

            console.log("User verified: " + verified);
            //Sends a boolean object back to app
            res.jsonp({ verified: verified });
        });
    }

    //Method receive post request from android app
    public changePassword = (req: express.Request, res: express.Response) => {
        var deviceNumber = req.body.deviceNumber;
        var password = req.body.password;

        console.log(deviceNumber + " " + password);

        var bcrypt = require('bcrypt');
        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(password, salt);

        this.userDAL.changePassword(deviceNumber, hash, (result) => {
            if (result.result.nModified == 1) {
                console.log("password change success");
                res.jsonp({ changed: true });
            } else {
                console.log("password change failed");
                res.jsonp({ changed: false });
            }
        });  
    }
}