﻿import {SemoDAL} from '../dal/semoDAL';
import {SEMODataParser} from '../parser/semoDataParser';
import {SEMO} from '../model/semo';
import http = require('http');
import express = require('express');

export class SemoController {

    public semoDAL: SemoDAL;

    constructor(semoDAL: SemoDAL) {
        this.semoDAL = semoDAL;
    }

    public getSemoData = (fromDate: string, toDate: string) => {
        var URL: string = 'http://semorep.sem-o.com/DataCollection/DataSets.asmx/queryDatasetXML?' +
            'DatasetName=EP2_RESULTS&User=kelvin-049@hotmail.com&Password=o212345&' +
            'FromDate=' + fromDate + '&ToDate=' + toDate + '&P1=WD1&P2=&P3=&P4=&P5=';

        console.log("URL: ", URL);

        //Fires a http request to get semo data
        http.get(URL, (res) => {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });

            res.on('end', () => {
                //Sends the response to an XML parser
                var semoDataParser = new SEMODataParser();
                semoDataParser.parseSemoData(body, (semoArray: Array<SEMO>) => {
                    //Sends the parsed data which is an array of SEMO objects to the database to be stored.
                    this.semoDAL.storeSemoData(semoArray);
                });
            });
        }).on('error', function (e) {
            console.log("Got an error on get semo: ", e);
        });
    }

    public getPrice = (req: express.Request, res: express.Response) => {
        console.log("Get price request from app: ", new Date());
        this.semoDAL.getPrice(new Date(), (price) => {

            console.log("Last price: ", price[price.length - 1].date, " ", price[price.length - 1].smp);
            res.jsonp({price: price[price.length - 1].smp});
        });
    }
}