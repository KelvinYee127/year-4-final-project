﻿import {ISemo} from '../interface/semoInterface';

export class SEMO implements ISemo{
    public currency: string;
    public date: Date;
    public runType: string;
    public smp: number;
    public lambda: number;
    public systemLoad: number;
    public cmsTimeStamp: string;

    constructor(currency: string, date: Date, runType: string, smp: number, lambda: number, systemLoad: number, cmsTimeStamp: string) {
        this.currency = currency;
        this.date = date;
        this.runType = runType;
        this.smp = smp;
        this.lambda = lambda;
        this.systemLoad = systemLoad;
        this.cmsTimeStamp = cmsTimeStamp;
    }
}