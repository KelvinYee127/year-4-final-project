﻿import {IOEM} from '../interface/oemInterface';

export class OEM implements IOEM {
    public deviceNumber: string;
    public name: string;
    public time: Date;
    public value: number;
    
    constructor(deviceNumber: string, name: string, time: Date, value: number) {
        this.deviceNumber = deviceNumber;
        this.name = name;
        this.time = time;
        this.value = value;
    }
}