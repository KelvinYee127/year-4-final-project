﻿import {IUser} from '../interface/userInterface';

export class User implements IUser{
    public email: string;
    public deviceNumber: string;
    public password: string;

    constructor(email: string, deviceNumber: string, password: string) {
        this.email = email;
        this.deviceNumber = deviceNumber;
        this.password = password;
    }

    public getEmail = (): string =>{
        return this.email;
    }

    public getDeviceNumber = (): string => {
        return this.deviceNumber;
    }

    public getPassword = (): string => {
        return this.password;
    }
}