"use strict";
class OEM {
    constructor(deviceNumber, name, time, value) {
        this.deviceNumber = deviceNumber;
        this.name = name;
        this.time = time;
        this.value = value;
    }
}
exports.OEM = OEM;
