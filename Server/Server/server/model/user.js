"use strict";
class User {
    constructor(email, deviceNumber, password) {
        this.getEmail = () => {
            return this.email;
        };
        this.getDeviceNumber = () => {
            return this.deviceNumber;
        };
        this.getPassword = () => {
            return this.password;
        };
        this.email = email;
        this.deviceNumber = deviceNumber;
        this.password = password;
    }
}
exports.User = User;
