"use strict";
class SEMO {
    constructor(currency, date, runType, smp, lambda, systemLoad, cmsTimeStamp) {
        this.currency = currency;
        this.date = date;
        this.runType = runType;
        this.smp = smp;
        this.lambda = lambda;
        this.systemLoad = systemLoad;
        this.cmsTimeStamp = cmsTimeStamp;
    }
}
exports.SEMO = SEMO;
