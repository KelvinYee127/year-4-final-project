﻿export interface ISemo {
    currency: string;
    date: Date;
    runType: string;
    smp: number;
    lambda: number;
    systemLoad: number;
    cmsTimeStamp: string;
}