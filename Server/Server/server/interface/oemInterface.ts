﻿export interface IOEM {
    deviceNumber: string;
    name: string;
    time: Date;
    value: number;
}