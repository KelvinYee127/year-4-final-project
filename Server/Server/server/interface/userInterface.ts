﻿export interface IUser {
    email: string;
    deviceNumber: string;
    password: string;
}