﻿import http = require('http');
import xml2js = require('xml2js');
import {SEMO} from '../model/semo';

export class SEMODataParser {
    public parseSemoData = (xmlStr: string, callback: (semoArray: Array<SEMO>) => void) => {
        var parser = new xml2js.Parser();

        //Parse XML data into SEMO objects and store them into an array
        parser.parseString(xmlStr, function (error, semo) {
            console.log("semo length: ", semo.length);
            var semoArray = new Array<SEMO>();
            
            for (var i = 0; i < 96; i += 2) {
                var dateArray = semo.EP2_RESULTS.Table1[i].DELIVERY_DATE.toString().split("-");
                var interval = semo.EP2_RESULTS.Table1[i].DELIVERY_INTERVAL.toString();
                var minute = 30;

                if (interval == "1") {
                    minute = 0;
                }

                var date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2], parseInt(semo.EP2_RESULTS.Table1[i].DELIVERY_HOUR.toString()), minute, 0, 0);

                var semoObject: SEMO = new SEMO(
                    semo.EP2_RESULTS.Table1[i].CURRENCY_FLAG.toString(),
                    date,
                    semo.EP2_RESULTS.Table1[i].RUN_TYPE.toString(),
                    parseFloat(semo.EP2_RESULTS.Table1[i].SMP.toString()),
                    parseFloat(semo.EP2_RESULTS.Table1[i].LAMBDA.toString()),
                    parseFloat(semo.EP2_RESULTS.Table1[i].SYSTEM_LOAD.toString()),
                    semo.EP2_RESULTS.Table1[i].CMS_TIME_STAMP.toString());

                semoArray.push(semoObject);
            }
            console.log('array size: ', semoArray.length);
            callback(semoArray);
        });
    }
}