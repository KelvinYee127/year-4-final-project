﻿import mongodb = require('mongodb');
import {User} from '../model/user';
import {DBDAL} from './dbDAL';

export class UserDAL {
    public dbDAL: DBDAL;
    private tempUser: User;

    //Makes a reference to the database dal to use the connection
    constructor(dbDAL: DBDAL) {
        this.dbDAL = dbDAL;
    }

    //Get user by device number from the database
    public getUserByDeviceNumber = (deviceNumber: string, callback: (user: User) => void) => {
        this.dbDAL.database.collection('user').findOne({ deviceNumber: deviceNumber })
            .then(callback);
    }

    public insertUser = (jsonArray: Array<User>) => {
        this.dbDAL.database.collection('user').insertMany(jsonArray);
    }

    public changePassword = (deviceNumber: string, password: string, callback: (result) => void) => {
        this.dbDAL.database.collection('user').update({ deviceNumber: deviceNumber }, {
            $set: { password: password }
        }).then(callback);
    }
}