﻿import mongodb = require('mongodb');
import {DBDAL} from './dbDAL';
import {OEM} from '../model/oem';

export class OEMDAL {
    public dbDAL: DBDAL;

    //Makes a reference to the database dal to use the connection
    constructor(dbDAL: DBDAL) {
        this.dbDAL = dbDAL;
    }

    //Inserts an OEM object into the electricityusage collection in the database.
    public storeOEMData = (oemObject: OEM) => {
        this.dbDAL.database.collection('electricityusage').insert(oemObject);
    }

    //Get user's usage data
    public getUserUsage = (deviceNumber: string, callback: (usage) => void) => {
        this.dbDAL.database.collection('electricityusage').find({ deviceNumber: deviceNumber }).toArray()
            .then(callback);
    }

    //Get price - should use semo dal but have no access
    public getPrice = (date: Date, callback: (price) => void) => {
        this.dbDAL.database.collection('electricityprice').find({ date: { $lte: date } }).toArray()
            .then(callback);
    }
}