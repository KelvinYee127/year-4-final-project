"use strict";
class OEMDAL {
    //Makes a reference to the database dal to use the connection
    constructor(dbDAL) {
        //Inserts an OEM object into the electricityusage collection in the database.
        this.storeOEMData = (oemObject) => {
            this.dbDAL.database.collection('electricityusage').insert(oemObject);
        };
        //Get user's usage data
        this.getUserUsage = (deviceNumber, callback) => {
            this.dbDAL.database.collection('electricityusage').find({ deviceNumber: deviceNumber }).toArray()
                .then(callback);
        };
        //Get price - should use semo dal but have no access
        this.getPrice = (date, callback) => {
            this.dbDAL.database.collection('electricityprice').find({ date: { $lte: date } }).toArray()
                .then(callback);
        };
        this.dbDAL = dbDAL;
    }
}
exports.OEMDAL = OEMDAL;
