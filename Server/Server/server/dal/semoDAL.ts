﻿import mongodb = require('mongodb');
import {DBDAL} from './dbDAL';
import {SEMO} from '../model/semo';

export class SemoDAL {
    public dbDAL: DBDAL;

    //Makes a reference to the database dal to use the connection
    constructor(dbDAL: DBDAL) {
        this.dbDAL = dbDAL;
    }

    //Inserts an array of SEMO object into the electricityprice collection in the database.
    public storeSemoData = (semoArray: Array<SEMO>) => {
        for (var i = 0; i < semoArray.length; i++){
            this.dbDAL.database.collection('electricityprice').insert(semoArray[i]);
        }
    }

    public getPrice = (date: Date, callback: (price) => void) => {
        this.dbDAL.database.collection('electricityprice').find({ date: { $lte: date } }).toArray()
            .then(callback);
    }
}