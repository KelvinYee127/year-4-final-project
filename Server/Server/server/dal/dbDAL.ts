﻿import mongodb = require('mongodb');

var MongoClient = require('mongodb').MongoClient;

export class DBDAL {
    public database: mongodb.Db;

    //Opens a connection to mongodb
    public openDBConnection = (callback) => {
        //MongoClient.connect('mongodb://ema:ema127@ds056559.mlab.com:56559/energymonitor', (error, db: mongodb.Db) => {
        MongoClient.connect('mongodb://localhost:27017/energymonitor', (error, db: mongodb.Db) => {
            if (error) {
                console.log('Unable to connect to server.', error);
            } else {
                console.log('Connection to server success.');
                this.database = db;
                callback();
            }
        });
    };

    //Closes the database connection
    public closeDBConnection = () => {
        this.database.close();
    }
}