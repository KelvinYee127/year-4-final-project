"use strict";
class SemoDAL {
    //Makes a reference to the database dal to use the connection
    constructor(dbDAL) {
        //Inserts an array of SEMO object into the electricityprice collection in the database.
        this.storeSemoData = (semoArray) => {
            for (var i = 0; i < semoArray.length; i++) {
                this.dbDAL.database.collection('electricityprice').insert(semoArray[i]);
            }
        };
        this.getPrice = (date, callback) => {
            this.dbDAL.database.collection('electricityprice').find({ date: { $lte: date } }).toArray()
                .then(callback);
        };
        this.dbDAL = dbDAL;
    }
}
exports.SemoDAL = SemoDAL;
