package course.examples.Networking.AndroidHttpClientXML;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class XMLResponseHandler implements ResponseHandler<List<String>> {

	private static final String SMP_TAG = "SMP";
	private static final String RUNTYPE_TAG = "RUN_TYPE";
	private static final String SYSTEMLOAD_TAG = "SYSTEM_LOAD";
	private String systemLoad, runType, smp;
	private boolean mIsParsingSystemLoad, mIsParsingRunType, mIsParsingSMP;
	private final List<String> mResults = new ArrayList<String>();

	@Override
	public List<String> handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {
		try {

			// Create the Pull Parser
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser xpp = factory.newPullParser();

			// Set the Parser's input to be the XML document in the HTTP Response
			xpp.setInput(new InputStreamReader(response.getEntity()
					.getContent()));
			
			// Get the first Parser event and start iterating over the XML document 
			int eventType = xpp.getEventType();

			while (eventType != XmlPullParser.END_DOCUMENT) {

				if (eventType == XmlPullParser.START_TAG) {
					startTag(xpp.getName());
				} else if (eventType == XmlPullParser.END_TAG) {
					endTag(xpp.getName());
				} else if (eventType == XmlPullParser.TEXT) {
					text(xpp.getText());
				}
				eventType = xpp.next();
			}
			return mResults;
		} catch (XmlPullParserException e) {
		}
		return null;
	}

	public void startTag(String localName) {
		if (localName.equals(SYSTEMLOAD_TAG)) {
			mIsParsingSystemLoad = true;
		} else if (localName.equals(RUNTYPE_TAG)) {
			mIsParsingRunType = true;
		} else if (localName.equals(SMP_TAG)) {
			mIsParsingSMP = true;
		}
	}

	public void text(String text) {
		if (mIsParsingSystemLoad) {
			systemLoad = text.trim();
		} else if (mIsParsingRunType) {
			runType = text.trim();
		} else if (mIsParsingSMP) {
			smp = text.trim();
		}
	}

	public void endTag(String localName) {
		if (localName.equals(SYSTEMLOAD_TAG)) {
			mIsParsingSystemLoad = false;
		} else if (localName.equals(RUNTYPE_TAG)) {
			mIsParsingRunType = false;
		} else if (localName.equals(SMP_TAG)) {
			mIsParsingSMP = false;
		} else if (localName.equals("Table1")) {
			mResults.add(SMP_TAG + ":" + smp + "," + SYSTEMLOAD_TAG + ":"
					+ systemLoad + "," + RUNTYPE_TAG + ":" + runType);
			Log.i("test", smp);
			systemLoad = null;
			runType = null;
			smp = null;
		}
	}
}