package course.examples.Networking.AndroidHttpClientJSON;

import android.app.ListActivity;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class NetworkingAndroidHttpClientJSONActivity extends ListActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		new HttpGetTask().execute();
	}

	private class HttpGetTask extends AsyncTask<Void, Void, Void> {
		//all 3 fields
		private static final String URL = "https://emoncms.org/feed/list.json&apikey=a9094f51ec8a9d3b295ba636e53be021";
		//just power
		//private static final String URL = "https://emoncms.org/feed/timevalue.json?id=88136&apikey=a9094f51ec8a9d3b295ba636e53be021";

		AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
		InputStream inputStream = null;
		String result = "";
		ArrayList<String> resultList = new ArrayList<String>();

		@Override
		protected Void doInBackground(Void... params) {
			HttpGet request = new HttpGet(URL);
			HttpResponse response = null;
			try {
				response = mClient.execute(request);
			} catch (IOException e) {
				e.printStackTrace();
			}
			HttpEntity httpEntity = response.getEntity();
			try {
				inputStream = httpEntity.getContent();
				BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
				StringBuilder sBuilder = new StringBuilder();

				String line = null;
				while ((line = bReader.readLine()) != null) {
					sBuilder.append(line + "\n");
				}

				inputStream.close();
				result = sBuilder.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			//parse JSON data
			try {

				JSONArray jArray = new JSONArray(result);
				for (int i = 0; i < jArray.length(); i++) {

					JSONObject jObject = jArray.getJSONObject(i);

					String name = jObject.getString("name");
					String value = jObject.getString("value");
					resultList.add(name + ", " + value);
					setListAdapter(new ArrayAdapter<String>(
							NetworkingAndroidHttpClientJSONActivity.this,
							R.layout.list_item, resultList));
					Log.i("name: ", name);
					Log.i("value: ", value);
				} // End Loop
			} catch (JSONException e) {
				Log.e("JSONException", "Error: " + e.toString());
			}
		}
	}
}


