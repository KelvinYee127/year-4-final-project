package com.example.blackie.energymonitor.Model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

public class SettingsListAdapter extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] settingTitles;
    private final String[] settingBodys;
    private final Integer[] iconID;

    SharedPreference dataFile;

    public SettingsListAdapter(Activity context, String[] settingTitles, String[] settingBodys, Integer[] iconID){
        super(context, R.layout.switch_layout, settingTitles);
        this.context = context;
        this.settingTitles = settingTitles;
        this.settingBodys = settingBodys;
        this.iconID = iconID;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        dataFile = new SharedPreference();

        LayoutInflater layoutInflater = this.context.getLayoutInflater();
        View rowView = null;

        if(position == 0) {
            rowView = layoutInflater.inflate(R.layout.switch_layout, null, true);

            ImageView notificationIcon = (ImageView) rowView.findViewById(R.id.switchLayoutIcon);
            TextView notificationTitle = (TextView) rowView.findViewById(R.id.switchLayoutTitle);
            TextView notificationBody = (TextView) rowView.findViewById(R.id.switchLayoutBody);
            Switch notificationSwitch = (Switch) rowView.findViewById(R.id.switchButton);

            notificationIcon.setImageResource(iconID[position]);
            notificationTitle.setText(settingTitles[position]);
            notificationBody.setText(settingBodys[position]);
            notificationSwitch.setChecked(dataFile.getBoolean(getContext(), "notification"));

            notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    dataFile.save(getContext(), "notification", isChecked);
                }
            });
        }else if(position == 1){
            rowView = layoutInflater.inflate(R.layout.dialog_layout, null, true);

            ImageView changePasswordIcon = (ImageView) rowView.findViewById(R.id.dialogLayoutIcon);
            TextView changePasswordTitle = (TextView) rowView.findViewById(R.id.dialogLayoutTitle);
            TextView changePasswordBody = (TextView) rowView.findViewById(R.id.dialogLayoutBody);

            changePasswordIcon.setImageResource(iconID[position]);
            changePasswordTitle.setText(settingTitles[position]);
            changePasswordBody.setText(settingBodys[position]);
        }
        return rowView;
    }
}
