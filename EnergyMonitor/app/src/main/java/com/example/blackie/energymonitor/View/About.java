package com.example.blackie.energymonitor.View;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.blackie.energymonitor.R;

public class About extends BaseMenu {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_about, contentFrameLayout);
    }
}
