package com.example.blackie.energymonitor.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.blackie.energymonitor.Utility.Constants;
import com.example.blackie.energymonitor.Controller.PasswordSender;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ChangePassword extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        final EditText newPassword = (EditText) findViewById(R.id.newPassword);
        final EditText confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        final Button changePasswordButton = (Button) findViewById(R.id.changePasswordButton);
        final TextView changePasswordError = (TextView) findViewById(R.id.changePasswordError);

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreference dataFile = new SharedPreference();

                String newPasswordStr = newPassword.getText().toString();
                String confirmPasswordStr = confirmPassword.getText().toString();

                if(newPasswordStr.equals("") || confirmPasswordStr.equals("")){
                    changePasswordError.setText(R.string.loginError);
                }else{
                    if(newPasswordStr.equals(confirmPasswordStr)){
                        URL url;

                        try {
                            url = new URL(Constants.SERVER_IP + "/changePassword");

                            Map<String, String> params = new HashMap<>();

                            params.put("deviceNumber", dataFile.getString(getApplicationContext(), "deviceNumber"));
                            params.put("password", newPasswordStr);

                            PasswordSender passwordSender = new PasswordSender(getApplicationContext(), url, params);
                            passwordSender.execute();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }else{
                        changePasswordError.setText(R.string.changePasswordError);
                    }
                }
            }
        });
    }
}
