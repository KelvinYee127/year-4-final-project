package com.example.blackie.energymonitor.View;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.blackie.energymonitor.Controller.GetHistoryDataSender;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.Constants;
import com.example.blackie.energymonitor.Utility.SharedPreference;
import com.github.mikephil.charting.charts.BarChart;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class History extends BaseMenu {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_history, contentFrameLayout);

        final SharedPreference dataFile = new SharedPreference();
        final BarChart barChartXML = (BarChart) findViewById(R.id.historyChart);
        Spinner chartSelection = (Spinner) findViewById(R.id.chartSelection);
        final TextView chartTitle = (TextView) findViewById(R.id.chartTitle);

        chartSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                URL url = null;
                try {
                    url = new URL(Constants.SERVER_IP + "/getHistoryData");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                Map<String, String> params = new HashMap<>();
                params.put("deviceNumber", dataFile.getString(getApplicationContext(), "deviceNumber"));

                if (position == 0){
                    params.put("selection", "day");
                }else if(position == 1){
                    params.put("selection", "week");
                }else{
                    params.put("selection", "year");
                }

                GetHistoryDataSender getHistoryDataSender = new GetHistoryDataSender(url, params, barChartXML);
                getHistoryDataSender.execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set clickable action for image in price text view
        chartTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= chartTitle.getRight() - chartTitle.getTotalPaddingRight()){
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the popup window layout
                        View popupWindowLayout = inflater.inflate(R.layout.popup_window_layout, null);

                        TextView popupWindowTextView = (TextView)popupWindowLayout.findViewById(R.id.popupTextView);

                        popupWindowTextView.setText(R.string.historyChartHelp);

                        Resources r = getResources();
                        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 130, r.getDisplayMetrics());

                        ViewGroup.LayoutParams layoutParams = popupWindowTextView.getLayoutParams();
                        layoutParams.width = Math.round(px);
                        popupWindowTextView.setLayoutParams(layoutParams);

                        final PopupWindow popupWindow = new PopupWindow(popupWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        popupWindow.setFocusable(true);

                        popupWindow.showAtLocation(contentFrameLayout, Gravity.START, Math.round(chartTitle.getX()) + chartTitle.getWidth(), -270);
                        return true;
                    }
                }
                return true;
            }
        });
    }
}
