package com.example.blackie.energymonitor.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.blackie.energymonitor.Utility.ParameterParser;
import com.example.blackie.energymonitor.View.Settings;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class PasswordSender extends AsyncTask<Void, Void, Boolean> {
    private Context context;
    private URL url;
    private Map<String, String> params;
    private ParameterParser parameterParser;

    public PasswordSender(Context context, URL url, Map<String, String> params){
        this.context = context;
        this.url = url;
        this.params = params;

        this.parameterParser = new ParameterParser();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        Boolean changed = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            if(urlConnection != null){
                //Parse and convert the supplied parameters into bytes
                String postData = this.parameterParser.parseParams(this.params);
                byte[] postDataBytes = postData.getBytes("UTF-8");

                //Set connection settings
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                //POST the data
                urlConnection.getOutputStream().write(postDataBytes);

                //Get the response code
                int responseCode = urlConnection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + postData);
                System.out.println("Response Code : " + responseCode);

                //Set up BufferedReader to capture response from the request
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuilder responseString = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    responseString.append(inputLine);
                }
                in.close();

                JSONObject response = new JSONObject(responseString.toString());
                changed = response.getBoolean("changed");
            }else{
                System.out.println("Connection to server fail in password sender.");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return changed;
    }

    @Override
    protected void onPostExecute(Boolean changed) {
        super.onPostExecute(changed);

        if(changed){
            SharedPreference dataFile = new SharedPreference();
            dataFile.save(this.context, "password", this.params.get("password"));
        }

        Intent intent = new Intent(this.context, Settings.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.context.startActivity(intent);
    }
}
