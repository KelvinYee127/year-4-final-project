package com.example.blackie.energymonitor.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Model.SettingsListAdapter;

public class Settings extends BaseMenu {
    ListView settingsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_settings, contentFrameLayout);

        settingsList = (ListView)findViewById(R.id.settingsList);

        String[] settingsTitles = getResources().getStringArray(R.array.settingsTitles);
        String[] settingsBodys = getResources().getStringArray(R.array.settingsBodys);
        Integer[] iconID = {R.mipmap.ic_priority_high_black_24dp, R.mipmap.ic_security_black_24dp};

        SettingsListAdapter settingsListAdapter = new SettingsListAdapter(this, settingsTitles, settingsBodys, iconID);
        settingsList.setAdapter(settingsListAdapter);

        settingsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1){
                    Intent changePasswordIntent = new Intent(getApplicationContext(), ChangePassword.class);
                    startActivity(changePasswordIntent);
                }
            }
        });
    }
}
