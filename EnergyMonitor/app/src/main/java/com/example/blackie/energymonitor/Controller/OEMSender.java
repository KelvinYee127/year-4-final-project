package com.example.blackie.energymonitor.Controller;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import com.example.blackie.energymonitor.Utility.ParameterParser;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class OEMSender extends AsyncTask<Void, Void, Integer> {
    private Context context;
    private URL url;
    private Map<String, String> params;
    private TextView textView;
    private ParameterParser parameterParser;

    public OEMSender(Context context, URL url, Map<String, String> params, TextView textView){
        this.context = context;
        this.url = url;
        this.params = params;
        this.textView = textView;

        this.parameterParser = new ParameterParser();
    }

    @Override
    protected Integer doInBackground(Void... params) {
        //Declare HTTP connection
        HttpURLConnection urlConnection = null;
        int usage = 0;

        try {
            //Set up connection to the supplied URL
            urlConnection = (HttpURLConnection) url.openConnection();
            if(urlConnection != null){
                //Parse and convert the supplied parameters into bytes
                String postData = parameterParser.parseParams(this.params);
                byte[] postDataBytes = postData.getBytes("UTF-8");

                //Set connection settings
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                //POST the data
                urlConnection.getOutputStream().write(postDataBytes);

                //Get the response code
                int responseCode = urlConnection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + postData);
                System.out.println("Response Code : " + responseCode);

                //Set up BufferedReader to capture response from the request
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuilder responseString = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    responseString.append(inputLine);
                }
                in.close();

                JSONObject response = new JSONObject(responseString.toString());
                usage = response.getInt("usage");
            }else{
                System.out.println("Connection to server fail in oem sender.");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
        }
        return usage;
    }

    @Override
    protected void onPostExecute(Integer usage) {
        super.onPostExecute(usage);

        SharedPreference dataFile = new SharedPreference();
        dataFile.save(this.context, "averageUsage", usage);
        textView.setText(this.context.getString(R.string.usage, String.valueOf(usage)));
        System.out.println("Average usage back from server: " + usage);
    }
}
