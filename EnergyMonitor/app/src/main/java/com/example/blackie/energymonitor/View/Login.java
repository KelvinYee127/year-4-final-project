package com.example.blackie.energymonitor.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.blackie.energymonitor.Utility.Constants;
import com.example.blackie.energymonitor.Controller.UserSender;
import com.example.blackie.energymonitor.R;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get references to EditText Object
                final EditText emailView = (EditText)findViewById(R.id.email);
                final EditText deviceNumberView = (EditText)findViewById(R.id.deviceNumber);
                final EditText passwordView = (EditText)findViewById(R.id.password);

                //Get values from EditText
                final String email = emailView.getText().toString();
                final String deviceNumber = deviceNumberView.getText().toString();
                final String password = passwordView.getText().toString();

                final TextView loginErrorView = (TextView)findViewById(R.id.loginErrorView);

                if(email.equals("") || deviceNumber.equals("") || password.equals("")){
                    loginErrorView.setText(R.string.loginError);
                }else{
                    //URL to server
                    URL url = null;

                    try {
                        url = new URL(Constants.SERVER_IP + "/login");
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                    //HashMap to store parameters to post as key value pairs
                    Map<String, String> userMap = new HashMap<>();

                    userMap.put("email", email);
                    userMap.put("deviceNumber", deviceNumber);
                    userMap.put("password", password);

                    //UserSender class handles all POST request and response
                    UserSender userSender = new UserSender(getApplicationContext(), url, userMap, loginErrorView);
                    userSender.execute();
                }
            }
        });

        final Button demoButton = (Button)findViewById(R.id.demoButton);
        demoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Demo.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, Intro.class));
        finish();
    }
}
