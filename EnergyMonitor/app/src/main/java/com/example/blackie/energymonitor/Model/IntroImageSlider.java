package com.example.blackie.energymonitor.Model;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.blackie.energymonitor.R;

public class IntroImageSlider extends PagerAdapter {
    private int[] imageResources = {R.drawable.app, R.drawable.carbon, R.drawable.pig};
    private Context context;
    private LayoutInflater layoutInflater;

    public IntroImageSlider(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.imageResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.intro_image_layout, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.introImageView);
        imageView.setImageResource(imageResources[position]);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}
