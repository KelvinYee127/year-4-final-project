package com.example.blackie.energymonitor.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

/*
    Source: https://www.youtube.com/watch?v=UH2OWnq7NQ4&t=2s Accessed: 8th February 2017
    Source: https://www.youtube.com/watch?v=J8mxBp5sBHg&t=184s Accessed: 20th March 2017
*/

public class CoinAnimation extends SurfaceView implements Runnable{

    //Threading Components
    Thread thread = null;
    boolean canDraw = false;

    //Animation Components
    //Platform
    Context context;
    Rect platform;
    int platformX;
    int platformY;
    int lengthOfPlatform;
    int thicknessOfPlatform;
    int pixelPerCent;
    int droppingDistance;

    //Coin
    Bitmap coin;
    int coinX;
    int coinY;
    int oldCoinX;
    int extraCoinX;

    //Canvas/Drawing Components
    Canvas canvas;
    SurfaceHolder surfaceHolder;
    Paint grayFillBrush;

    //Device/Screen Components
    DisplayMetrics displayMetrics;
    int screenHeight;
    int screenWidth;
    int halfScreenHeight;

    //Loop variables
    double framePerSecond;
    double frameTimeSeconds;
    double frameTimeMS;
    double frameTimeNS;
    double timeLastFrame;
    double timeEndOfDraw;
    double deltaTime;

    SharedPreference dataFile = new SharedPreference();

    public CoinAnimation(Context context){
        super(context);

        this.context = context;
        surfaceHolder = getHolder();

        //Get screen width and height
        displayMetrics = context.getResources().getDisplayMetrics();
        screenHeight = displayMetrics.heightPixels;
        halfScreenHeight = screenHeight / 2;
        screenWidth = displayMetrics.widthPixels;

        //Set the maximum price at 1euro which is 100cents
        pixelPerCent = Math.round(screenWidth / 100);

        //Set variables for platform and initialise
        platformX = 0;
        platformY = halfScreenHeight;
        lengthOfPlatform = 1000; //Change according to the calculation
        thicknessOfPlatform = halfScreenHeight + 50;
        platform = new Rect(platformX, platformY, lengthOfPlatform, thicknessOfPlatform);

        droppingDistance = 180;

        //Initialise coin and set variables
        coin = BitmapFactory.decodeResource(getResources(), R.mipmap.coin);
        coinX = -10;
        coinY = halfScreenHeight - coin.getHeight();
        extraCoinX = 100;

        //Initialise Loop variables
        framePerSecond = 1;
        frameTimeSeconds = 1 / framePerSecond;
        frameTimeMS = frameTimeSeconds * 1000;
        frameTimeNS = frameTimeMS * 1000000;
    }

    @Override
    public void run() {
        prepPaintBrushes();

        timeLastFrame = System.nanoTime();
        deltaTime = 0;

        while(canDraw){
            if(!surfaceHolder.getSurface().isValid()){
                continue;
            }

            lockCanvas();
            canvas.drawColor(Color.WHITE);
            drawPlatform();
            int coinRollingTime = calculation();
            int speed = (lengthOfPlatform + droppingDistance) / coinRollingTime;
            //System.out.println("Speed: " + speed);

            Matrix matrix = new Matrix();
            matrix.postRotate(90);

            coin = Bitmap.createBitmap(coin, 0, 0, coin.getWidth(), coin.getHeight(), matrix, true);

            moveCoin(speed);
            canvas.drawBitmap(coin, coinX, coinY, null);

            dataFile.save(this.context, "coinRollingTime", coinRollingTime);

            String displayString = "1 cent last " + String.valueOf(coinRollingTime) + " seconds";
            canvas.drawText(displayString, 700, 320, grayFillBrush);
            unlockCanvas();

            //After drawing
            timeEndOfDraw = System.nanoTime();

            //Calculate deltaTime
            deltaTime = frameTimeNS - (timeEndOfDraw - timeLastFrame);

            //printTimes();

            //Sleep/Delay the frame for deltaTime
            try {
                if(deltaTime > 0) {
                    thread.sleep((long) (deltaTime / 1000000));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            timeLastFrame = System.nanoTime();
        }
    }

    public void pause() {
        canDraw = false;

        while (true){
            try {
                thread.join();
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        thread = null;
    }

    public void resume(){
        canDraw = true;
        thread = new Thread(this);
        thread.start();
    }

    private void prepPaintBrushes(){
        grayFillBrush = new Paint();
        grayFillBrush.setColor(Color.DKGRAY);
        grayFillBrush.setStyle(Paint.Style.FILL);
        grayFillBrush.setTextSize(50);
    }

    private void lockCanvas(){
        canvas = surfaceHolder.lockCanvas();
    }

    private void unlockCanvas(){
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    private void moveCoin(int speed){
        oldCoinX = coinX;
        coinX += speed;
        if(coinY <= screenHeight) {
            //System.out.println("Im here");
            if (coinX > lengthOfPlatform) {
                coinX = oldCoinX + extraCoinX;
                if(coinX >= lengthOfPlatform) {
                    int extraCoinXAfterDecrement = extraCoinX - 25;

                    if(extraCoinXAfterDecrement <= 0){
                        extraCoinX = 0;
                    }else {
                        extraCoinX -= 25;
                    }
                    //System.out.println("Length, x, extra: " + lengthOfPlatform + " " + coinX + " " + extraCoinX);
                }
                coinY += 100;
                //System.out.println("Im here 2");
            }
        }else{
            coinX = -10;
            coinY = halfScreenHeight - coin.getHeight();
            extraCoinX = 100;
        }
    }

    private void drawPlatform(){
        float price = dataFile.getFloat(this.context, "price");

        if(price >  100){
            price = 100;
        }

        lengthOfPlatform = screenWidth - Math.round(price * pixelPerCent);

        platform = new Rect(platformX, platformY, lengthOfPlatform, thicknessOfPlatform);

        //System.out.println("Length of platform: " + price + " " + pixelPerCent + " " + lengthOfPlatform);

        canvas.drawRect(platform, grayFillBrush);
    }

    private int calculation(){
        int usage = dataFile.getInt(this.context, "averageUsage");
        float price = dataFile.getFloat(this.context, "price");

        if(price > 100){
            price = 100;
        }

        //System.out.println("Usage: " + usage + " Price: " + price);
        //1kWh divide by cent/kWh
        float wattsHourInOneCent = 1000 / price;
        float totalCostForOneHour = usage / wattsHourInOneCent;
        //60 minute in one hour
        float durationOfOneCent = 60 / totalCostForOneHour;
        //Get duration in seconds
        int durationOfOneCentSeconds = Math.round(durationOfOneCent * 60);

        //System.out.println("Figures: " + wattsHourInOneCent + " " + totalCostForOneHour + " " + durationOfOneCent + " " + durationOfOneCentSeconds + " " + Math.round(durationOfOneCent));
        //System.out.println("Screen Height and Width: " + screenHeight + " " + screenWidth);
        //Minutes in seconds
        return durationOfOneCentSeconds;
    }

    private void printTimes(){
        System.out.println("Frames per sec: " + framePerSecond);
        System.out.println("Frame time sec: " + frameTimeSeconds);
        System.out.println("Frame time ms: " + frameTimeMS);
        System.out.println("Frame time ns: " + frameTimeNS);
        System.out.println("Time Last Frame: " + timeLastFrame);
        System.out.println("Time end of draw: " + timeEndOfDraw);
        System.out.println("Delta Time: " + deltaTime);
        System.out.println("Delta Time sec: " + deltaTime / 1000000000);
    }
}
