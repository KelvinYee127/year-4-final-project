package com.example.blackie.energymonitor.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {
    public static final String FILE_NAME = "data";

    public SharedPreference(){
        super();
    }

    public void save(Context context, String key, String value){
        SharedPreferences dataFile;
        SharedPreferences.Editor editor;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        editor = dataFile.edit();

        editor.putString(key, value);
        editor.apply();
    }

    public void save(Context context, String key, int value){
        SharedPreferences dataFile;
        SharedPreferences.Editor editor;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        editor = dataFile.edit();

        editor.putInt(key, value);
        editor.apply();
    }

    public void save(Context context, String key, float value){
        SharedPreferences dataFile;
        SharedPreferences.Editor editor;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        editor = dataFile.edit();

        editor.putFloat(key, value);
        editor.apply();
    }

    public void save(Context context, String key, boolean value){
        SharedPreferences dataFile;
        SharedPreferences.Editor editor;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        editor = dataFile.edit();

        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getString(Context context, String key){
        SharedPreferences dataFile;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return dataFile.getString(key, null);
    }

    public int getInt(Context context, String key){
        SharedPreferences dataFile;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return dataFile.getInt(key, -1);
    }

    public float getFloat(Context context, String key){
        SharedPreferences dataFile;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return dataFile.getFloat(key, -1f);
    }

    public boolean getBoolean(Context context, String key){
        SharedPreferences dataFile;

        dataFile = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return dataFile.getBoolean(key, false);
    }
}
