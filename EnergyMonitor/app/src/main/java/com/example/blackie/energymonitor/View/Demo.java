package com.example.blackie.energymonitor.View;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.blackie.energymonitor.Model.CoinAnimationDemo;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import static com.example.blackie.energymonitor.R.string.price;
import static com.example.blackie.energymonitor.R.string.usage;

public class Demo extends DemoBaseClass {

    CoinAnimationDemo coinAnimationDemo;

    SharedPreference dataFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        coinAnimationDemo = new CoinAnimationDemo(this);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.demoAnimationFrame);
        contentFrameLayout.addView(coinAnimationDemo);
        getLayoutInflater().inflate(R.layout.activity_demo, contentFrameLayout);

        dataFile = new SharedPreference();
        //Put initial values into file
        dataFile.save(getApplicationContext(), "demoUsage", 6000);
        dataFile.save(getApplicationContext(), "demoPrice", 20f);

        SeekBar usageBar = (SeekBar) findViewById(R.id.usageBar);
        SeekBar priceBar = (SeekBar) findViewById(R.id.priceBar);
        final TextView usageText = (TextView) findViewById(R.id.usageText);
        final TextView priceText = (TextView) findViewById(R.id.priceText);

        usageText.setText(getString(usage, String.valueOf(usageBar.getProgress())));
        priceText.setText(getString(price, String.valueOf(priceBar.getProgress())));

        usageBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int usage;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                usage = progress;
                usageText.setText("Usage: " + progress + "watts");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                dataFile.save(getApplicationContext(), "demoUsage", usage);
            }
        });

        priceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            float price;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                price = progress;
                priceText.setText("Price: " + progress + "cents");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                dataFile.save(getApplicationContext(), "demoPrice", price);
            }
        });
    }

    @Override
    protected void onPause(){
        super.onPause();
        coinAnimationDemo.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        coinAnimationDemo.resume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
