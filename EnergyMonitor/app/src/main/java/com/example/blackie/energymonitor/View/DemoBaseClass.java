package com.example.blackie.energymonitor.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.blackie.energymonitor.R;

public class DemoBaseClass extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_base_class);
    }
}
