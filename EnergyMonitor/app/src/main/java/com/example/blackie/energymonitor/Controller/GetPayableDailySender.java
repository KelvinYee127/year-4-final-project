package com.example.blackie.energymonitor.Controller;


import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.ParameterParser;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class GetPayableDailySender extends AsyncTask<Void, Void, Double> {

    private Context context;
    private URL url;
    private Map<String, String> params;
    private TextView textView;
    private ParameterParser parameterParser;

    public GetPayableDailySender(Context context, URL url, Map<String, String> params, TextView textView) {
        this.context = context;
        this.url = url;
        this.params = params;
        this.textView = textView;

        this.parameterParser = new ParameterParser();
    }

    @Override
    protected Double doInBackground(Void... params) {
        //Declare HTTP connection
        HttpURLConnection urlConnection = null;
        double payable = 0;

        try {
            //Set up connection to the supplied URL
            urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection != null) {
                //Parse and convert the supplied parameters into bytes
                String postData = parameterParser.parseParams(this.params);
                byte[] postDataBytes = postData.getBytes("UTF-8");

                //Set connection settings
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                //POST the data
                urlConnection.getOutputStream().write(postDataBytes);

                //Get the response code
                int responseCode = urlConnection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + postData);
                System.out.println("Response Code : " + responseCode);

                //Set up BufferedReader to capture response from the request
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuilder responseString = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    responseString.append(inputLine);
                }
                in.close();

                JSONObject response = new JSONObject(responseString.toString());
                payable = response.getDouble("payable");
            } else {
                System.out.println("Connection to server fail in GetPayableDaily sender.");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return payable;
    }

    @Override
    protected void onPostExecute(Double payable) {
        super.onPostExecute(payable);

        SharedPreference dataFile = new SharedPreference();
        float payableFloat = Float.parseFloat(payable.toString());
        dataFile.save(this.context, "payable", payableFloat);
        textView.setText(this.context.getString(R.string.payable, String.valueOf(payable)));
        System.out.println("Payable back from server: " + payable);
    }
}
