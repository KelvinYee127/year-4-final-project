package com.example.blackie.energymonitor.Model;

import android.graphics.Color;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class HistoryChart {
    private ArrayList<BarEntry> chartData;
    private BarChart historyChart;

    public HistoryChart(BarChart historyChart, ArrayList<BarEntry> chartData){
        this.historyChart = historyChart;
        this.chartData = chartData;
    }

    public BarChart createChart(){
        this.historyChart.clear();
        BarDataSet barDataSet = new BarDataSet(this.chartData, "Usage in kWh");
        BarData barData = new BarData(barDataSet);
        this.historyChart.setData(barData);

        barDataSet.setColor(Color.parseColor("#1582c0"));

        this.historyChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        this.historyChart.getXAxis().setDrawGridLines(false);

        this.historyChart.getAxisLeft().setAxisMinimum(0f);

        this.historyChart.getAxisRight().setEnabled(false);
        this.historyChart.getLegend().setEnabled(false);

        Description chartDescription = new Description();
        chartDescription.setText("");
        this.historyChart.setDescription(chartDescription);

        return this.historyChart;
    }

    public void setXAxis(ArrayList<String> xLabels){
        this.historyChart.getXAxis().setValueFormatter(new MyXAxisValueFormatter(xLabels));
    }

    public void setYAxis(){
        this.historyChart.getAxisLeft().setValueFormatter(new MyYAxisValueFormatter());
    }

    private class MyXAxisValueFormatter implements IAxisValueFormatter {
        private ArrayList<String> labels;

        public MyXAxisValueFormatter(ArrayList<String> labels) {
            this.labels = labels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            // "value" represents the position of the label on the axis (x or y)
            if(this.labels.size() > (int) value) {
                System.out.println("Position of label: " + value + " Value: " + this.labels.get((int) value));
                return this.labels.get((int) value);
            }else{
                return null;
            }
        }
    }

    private class MyYAxisValueFormatter implements IAxisValueFormatter {
        public MyYAxisValueFormatter() {
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            // "value" represents the position of the label on the axis (x or y)
            return Math.round(value) + "W";
        }
    }
}
