package com.example.blackie.energymonitor.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;

import com.example.blackie.energymonitor.View.Home;
import com.example.blackie.energymonitor.Utility.ParameterParser;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class UserSender extends AsyncTask<Void,Void, Boolean> {
    private Context context;
    private URL url;
    private Map<String, String> params;
    private TextView errorView;
    private ParameterParser parameterParser;

    public UserSender(Context context, URL url, Map<String, String> params, TextView errorView){
        this.context = context;
        this.url = url;
        this.params = params;
        this.errorView = errorView;
        this.parameterParser = new ParameterParser();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        //Declare HTTP connection
        HttpURLConnection urlConnection = null;
        Boolean verified = null;
        try {
            //Set up connection to the supplied URL
            urlConnection = (HttpURLConnection) url.openConnection();
            if(urlConnection != null){
                //Parse and convert the supplied parameters into bytes
                String postData = parameterParser.parseParams(this.params);
                byte[] postDataBytes = postData.getBytes("UTF-8");

                //Set connection settings
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                //POST the data
                urlConnection.getOutputStream().write(postDataBytes);

                //Get the response code
                int responseCode = urlConnection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + postData);
                System.out.println("Response Code : " + responseCode);

                //Set up BufferedReader to capture response from the request
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuilder responseString = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    responseString.append(inputLine);
                }
                in.close();

                //Get response object
                JSONObject response = new JSONObject(responseString.toString());
                verified = response.getBoolean("verified");

                System.out.println("User verified : " + verified);
            }else{
                System.out.println("Connection to server fail in user sender.");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
        }
        return verified;
    }

    /**
     * This method would direct app to the home screen is user login is verified
     * @param verified
     */
    @Override
    protected void onPostExecute(Boolean verified) {
        super.onPostExecute(verified);
        if(verified){
            this.errorView.setText("");

            SharedPreference dataFile = new SharedPreference();

            dataFile.save(this.context, "deviceNumber", this.params.get("deviceNumber"));
            dataFile.save(this.context, "password", this.params.get("password"));

            Intent intent = new Intent(this.context, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        }else{
            this.errorView.setText(R.string.loginFail);
        }
    }
}
