package com.example.blackie.energymonitor.Controller;

import android.os.AsyncTask;

import com.example.blackie.energymonitor.Model.HistoryChart;
import com.example.blackie.energymonitor.Utility.ParameterParser;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

public class GetHistoryDataSender extends AsyncTask<Void, Void, JSONArray> {
    private URL url;
    private Map<String, String> params;
    private BarChart barChart;
    private ParameterParser parameterParser;

    public GetHistoryDataSender(URL url, Map<String, String> params, BarChart barChart){
        this.url = url;
        this.params = params;
        this.barChart = barChart;

        this.parameterParser = new ParameterParser();
    }

    @Override
    protected JSONArray doInBackground(Void... params) {
        //Declare HTTP connection
        HttpURLConnection urlConnection = null;
        JSONArray timeAndUsageArray = null;
        try {
            //Set up connection to the supplied URL
            urlConnection = (HttpURLConnection) url.openConnection();
            if(urlConnection != null){
                //Parse and convert the supplied parameters into bytes
                String postData = parameterParser.parseParams(this.params);
                byte[] postDataBytes = postData.getBytes("UTF-8");

                //Set connection settings
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                //POST the data
                urlConnection.getOutputStream().write(postDataBytes);

                //Get the response code
                int responseCode = urlConnection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + postData);
                System.out.println("Response Code : " + responseCode);

                //Set up BufferedReader to capture response from the request
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuilder responseString = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    responseString.append(inputLine);
                }
                in.close();

                JSONObject response = new JSONObject(responseString.toString());
                timeAndUsageArray = response.getJSONArray("usage");

                System.out.println("response from history: " + response.getJSONArray("usage"));
            }else{
                System.out.println("Connection to server fail in history data sender.");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
        }
        return timeAndUsageArray;
    }

    @Override
    protected void onPostExecute(JSONArray timeAndUsageArray) {
        super.onPostExecute(timeAndUsageArray);

        ArrayList<String> xLabels = new ArrayList<>();
        ArrayList<BarEntry> entries = new ArrayList<>();
        float barEntryCount = 0;

        if(this.params.get("selection").equals("day")) {
            for (int i = 0; i < timeAndUsageArray.length(); i++) {
                try {
                    String timeAndUsage = (String) timeAndUsageArray.get(i);
                    String[] splitArray = timeAndUsage.split(",");

                    int hour = Integer.valueOf(splitArray[0]);

                    if (hour > 12) {
                        hour -= 12;
                        xLabels.add(String.valueOf(hour) + "pm");
                    } else if (hour == 0) {
                        xLabels.add("12am");
                    } else if (hour == 12) {
                        xLabels.add("12pm");
                    } else {
                        xLabels.add(splitArray[0] + "am");
                    }

                    entries.add(new BarEntry(barEntryCount, Float.valueOf(splitArray[1])));
                    barEntryCount++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            HistoryChart barChart = new HistoryChart(this.barChart, entries);
            barChart.createChart();

            this.barChart.getXAxis().setLabelCount(xLabels.size());
            this.barChart.getXAxis().setTextSize(9f);
            barChart.setXAxis(xLabels);
            barChart.setYAxis();
        }else if(this.params.get("selection").equals("week")){
            for (int i = 0; i < timeAndUsageArray.length(); i++) {
                try {
                    String timeAndUsage = (String) timeAndUsageArray.get(i);
                    String[] splitArray = timeAndUsage.split(",");

                    xLabels.add(splitArray[0]);

                    entries.add(new BarEntry(barEntryCount, Float.valueOf(splitArray[1])));
                    barEntryCount++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            xLabels.add("Wednesday");
            xLabels.add("Thursday");
            xLabels.add("Friday");
            xLabels.add("Saturday");
            xLabels.add("Sunday");
            xLabels.add("Monday");

            entries.add(new BarEntry(1f, 5000f));
            entries.add(new BarEntry(2f, 3200f));
            entries.add(new BarEntry(3f, 4529f));
            entries.add(new BarEntry(4f, 6512f));
            entries.add(new BarEntry(5f, 8694f));
            entries.add(new BarEntry(6f, 7361f));

            HistoryChart barChart = new HistoryChart(this.barChart, entries);
            barChart.createChart();

            this.barChart.getXAxis().setLabelCount(xLabels.size());
            barChart.setXAxis(xLabels);
            barChart.setYAxis();
        }else{
            //Year data here
            //not enough data in database to write this function

//            xLabels.add("January");
//            xLabels.add("February");
//            xLabels.add("March");
//            xLabels.add("April");
//            xLabels.add("May");
//            xLabels.add("June");
//            xLabels.add("July");
//            xLabels.add("August");
//            xLabels.add("September");
//            xLabels.add("October");
//            xLabels.add("November");
//            xLabels.add("December");
        }
    }
}
