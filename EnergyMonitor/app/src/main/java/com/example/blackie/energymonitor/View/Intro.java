package com.example.blackie.energymonitor.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.blackie.energymonitor.Model.IntroImageSlider;
import com.example.blackie.energymonitor.R;

public class Intro extends AppCompatActivity {

    ViewPager viewPager;
    IntroImageSlider imageSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        imageSlider = new IntroImageSlider(this);
        viewPager.setAdapter(imageSlider);

        final Button introLoginButton = (Button)findViewById(R.id.introLoginButton);
        introLoginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(Intro.this, Login.class));
            }
        });
    }
}
