package com.example.blackie.energymonitor.Utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ParameterParser {

    public ParameterParser(){
        super();
    }

    /**
     * This method takes in a Map<String, String> of Parameters,
     * convert them into appropriate format for POST request.
     * @param paramsMap
     * @return properly formatted parameters in the form {Name=Value[&]...}
     * @throws UnsupportedEncodingException
     */
    public String parseParams(Map<String, String> paramsMap) throws UnsupportedEncodingException {
        StringBuilder postData = new StringBuilder();
        for (HashMap.Entry<String,String> param : paramsMap.entrySet()) {
            //If there is more than one parameter, append &
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
        }
        return postData.toString();
    }
}
