package com.example.blackie.energymonitor.View;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.blackie.energymonitor.Controller.GetPayableDailySender;
import com.example.blackie.energymonitor.Controller.OEMSender;
import com.example.blackie.energymonitor.Controller.SemoSender;
import com.example.blackie.energymonitor.Model.CoinAnimation;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.Constants;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class Home extends BaseMenu{

    CoinAnimation coinAnimation;

    // 30 seconds
    private static final long USAGE_UPDATE_INTERVAL = 30000;
    private static final long PRICE_UPDATE_INTERVAL = 60000;
    private static final long PAYABLE_UPDATE_INTERVAL = 300000;

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    SemoSender semoSender;
    URL semoURL;

    OEMSender oemSender;
    URL oemURL;
    HashMap<String, String> oemMap = new HashMap<>();

    GetPayableDailySender getPayableDailySender;
    URL getPayableURL;
    HashMap<String, String> getPayableMap = new HashMap<>();

    TextView usageView = null;
    TextView priceView = null;
    TextView payableView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        coinAnimation = new CoinAnimation(this);

        final FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        contentFrameLayout.addView(coinAnimation);
        getLayoutInflater().inflate(R.layout.activity_home, contentFrameLayout);

        usageView = (TextView)findViewById(R.id.usageView);
        priceView = (TextView)findViewById(R.id.priceView);
        payableView = (TextView)findViewById(R.id.payableView);

        final TextView animationHelp = (TextView)findViewById(R.id.animationHelp);

        final SharedPreference dataFile = new SharedPreference();

        //Set URLs
        try {
            oemURL = new URL(Constants.SERVER_IP + "/getUserUsageOneHour");
            semoURL = new URL(Constants.SERVER_IP + "/getPrice");
            getPayableURL = new URL(Constants.SERVER_IP + "/getPayable");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //Set param maps
        oemMap.put("deviceNumber", dataFile.getString(getApplicationContext(), "deviceNumber"));
        getPayableMap.put("deviceNumber", dataFile.getString(getApplicationContext(), "deviceNumber"));

        //Set clickable action for image in price text view
        usageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= usageView.getRight() - usageView.getTotalPaddingRight()){
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the popup window layout
                        View popupWindowLayout = inflater.inflate(R.layout.popup_window_layout, null);

                        TextView popupWindowTextView = (TextView)popupWindowLayout.findViewById(R.id.popupTextView);
                        popupWindowTextView.setText(R.string.usageHelp);


                        final PopupWindow popupWindow = new PopupWindow(popupWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        popupWindow.setFocusable(true);

                        popupWindow.showAtLocation(contentFrameLayout, Gravity.START, Math.round(usageView.getX()) + usageView.getWidth(), -250);
                        return true;
                    }
                }
                return true;
            }
        });

        //Set clickable action for image in price text view
        priceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= priceView.getRight() - priceView.getTotalPaddingRight()){
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the popup window layout
                        View popupWindowLayout = inflater.inflate(R.layout.popup_window_layout, null);

                        TextView popupWindowTextView = (TextView)popupWindowLayout.findViewById(R.id.popupTextView);
                        popupWindowTextView.setText(R.string.priceHelp);

                        final PopupWindow popupWindow = new PopupWindow(popupWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        popupWindow.setFocusable(true);

                        popupWindow.showAtLocation(contentFrameLayout, Gravity.START, Math.round(priceView.getX()) + priceView.getWidth(), -180);
                        return true;
                    }
                }
                return true;
            }
        });

        //Set clickable action for image in payable text view
        payableView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= payableView.getRight() - payableView.getTotalPaddingRight()){
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the popup window layout
                        View popupWindowLayout = inflater.inflate(R.layout.popup_window_layout, null);

                        TextView popupWindowTextView = (TextView)popupWindowLayout.findViewById(R.id.popupTextView);
                        popupWindowTextView.setText(R.string.payableHelp);

                        Resources r = getResources();
                        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, r.getDisplayMetrics());

                        ViewGroup.LayoutParams layoutParams = popupWindowTextView.getLayoutParams();
                        layoutParams.width = Math.round(px);
                        popupWindowTextView.setLayoutParams(layoutParams);

                        final PopupWindow popupWindow = new PopupWindow(popupWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        popupWindow.setFocusable(true);

                        popupWindow.showAtLocation(contentFrameLayout, Gravity.START, Math.round(payableView.getX()) + payableView.getWidth(), -250);
                        return true;
                    }
                }
                return true;
            }
        });

        //Set clickable action for image in animation text view
        animationHelp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= animationHelp.getRight() - animationHelp.getTotalPaddingRight()){
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the popup window layout
                        View popupWindowLayout = inflater.inflate(R.layout.popup_window_layout, null);

                        TextView popupWindowTextView = (TextView)popupWindowLayout.findViewById(R.id.popupTextView);
                        popupWindowTextView.setText(R.string.animationHelp);

                        int usage = dataFile.getInt(getApplicationContext(), "averageUsage");

                        int coinRollingTime = dataFile.getInt(getApplicationContext(), "coinRollingTime");

                        popupWindowTextView.setText(getApplicationContext().getString(R.string.animationHelp, String.valueOf(usage), String.valueOf(coinRollingTime)));

                        final PopupWindow popupWindow = new PopupWindow(popupWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        popupWindow.setFocusable(true);

                        popupWindow.showAtLocation(contentFrameLayout, Gravity.START, 1320, -180);
                        return true;
                    }
                }
                return true;
            }
        });

        //Set timer
        //cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        coinAnimation.pause();
        mTimer.cancel();
    }

    @Override
    protected void onResume(){
        super.onResume();

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new UsageDisplayTimerTask(), 0, USAGE_UPDATE_INTERVAL);
        mTimer.scheduleAtFixedRate(new PriceDisplayTimerTask(), 0, PRICE_UPDATE_INTERVAL);
        mTimer.scheduleAtFixedRate(new PayableDisplayTimerTask(), 0, PAYABLE_UPDATE_INTERVAL);

        coinAnimation.resume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTimer.cancel();
    }

    @Override
    public void onBackPressed() {

    }

    private class UsageDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    oemSender = new OEMSender(getApplicationContext(), oemURL, oemMap, usageView);
                    oemSender.execute();
                }

            });
        }
    }

    private class PriceDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    semoSender = new SemoSender(getApplicationContext(), semoURL, priceView);
                    semoSender.execute();
                }

            });
        }
    }

    private class PayableDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    getPayableDailySender = new GetPayableDailySender(getApplicationContext(), getPayableURL, getPayableMap, payableView);
                    getPayableDailySender.execute();
                }

            });
        }
    }
}
