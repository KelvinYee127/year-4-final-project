package com.example.blackie.energymonitor.Controller;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;

import com.example.blackie.energymonitor.Utility.Constants;
import com.example.blackie.energymonitor.View.Home;
import com.example.blackie.energymonitor.R;
import com.example.blackie.energymonitor.Utility.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.Context.NOTIFICATION_SERVICE;

public class SemoSender extends AsyncTask<Void,Void, Double> {
    private Context context;
    private URL url;
    private TextView priceView;

    public SemoSender(Context context, URL url, TextView priceView){
        this.context = context;
        this.url = url;
        this.priceView = priceView;
    }

    @Override
    protected Double doInBackground(Void... params) {
        //Declare HTTP connection
        HttpURLConnection urlConnection = null;
        double price = 0;

        try {
            //Set up connection to the supplied URL
            urlConnection = (HttpURLConnection) url.openConnection();
            if(urlConnection != null){
                //Set connection settings
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);

                //Connect to the URL
                urlConnection.connect();

                //Get the response code
                int responseCode = urlConnection.getResponseCode();

                Log.i("responseCode", String.valueOf(responseCode));
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Response Code : " + responseCode);

                //Set up BufferedReader to capture response from the request
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuilder responseString = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    responseString.append(inputLine);
                }
                in.close();

                JSONObject response = new JSONObject(responseString.toString());
                price = response.getDouble("price");
            }else{
                System.out.println("Connection to server fail in semo sender.");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
        }
        return price;
    }

    @Override
    protected void onPostExecute(Double price) {
        super.onPostExecute(price);

        SharedPreference dataFile = new SharedPreference();
        dataFile.save(this.context, "price", price.floatValue());

        if(dataFile.getBoolean(this.context, "notification")){
            if(price.floatValue() > Constants.MAX_PRICE){
                createNotification();
            }
        }
        this.priceView.setText(this.context.getString(R.string.price, String.valueOf(price)));
    }

    private void createNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this.context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Electricity price have gone over 40 cents!");

        Intent resultIntent = new Intent(this.context, Home.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this.context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);

        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) this.context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
